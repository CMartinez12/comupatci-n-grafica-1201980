
function contruirEdificios() {
    // Cread Edificios

    var cajacarreteraGeometria = new THREE.BoxGeometry(20, 0, 600);
    var cajacarreteraMaterial = new THREE.MeshBasicMaterial({ color: 16777215, wireframe: false });
    var cajacarretera = new THREE.Mesh(cajacarreteraGeometria, cajacarreteraMaterial);
    cajacarretera.position.x = 140;
    cajacarretera.position.y = 0;
    cajacarretera.position.z = 40;
    escenario.add(cajacarretera)

    var cajacarretera1Geometria = new THREE.BoxGeometry(200, 0, 20);
    var cajacarretera1Material = new THREE.MeshBasicMaterial({ color: 16777215, wireframe: false });
    var cajacarretera1 = new THREE.Mesh(cajacarretera1Geometria, cajacarretera1Material);
    cajacarretera1.position.x = 250;
    cajacarretera1.position.y = 0;
    cajacarretera1.position.z = 40;
    escenario.add(cajacarretera1)





    // aqui se enlazan todo lo otrso elementos 
    var cajaGeometria = new THREE.BoxGeometry(40, 40, 40);
    var cajaMaterial = new THREE.MeshBasicMaterial({ color: 0xd4760f, wireframe: false });
    var edificio = new THREE.Mesh(cajaGeometria, cajaMaterial);
    edificio.position.x = -10;
    edificio.position.y = 20;
    edificio.position.z = 0;
    escenario.add(edificio)



    var caja2Geometria = new THREE.BoxGeometry(40, 40, 40);
    var caja2Material = new THREE.MeshBasicMaterial({ color: 0xd4760f, wireframe: false });
    var caja2 = new THREE.Mesh(caja2Geometria, caja2Material);
    caja2.position.x = 0;
    caja2.position.y = 0;
    caja2.position.z = 40;
    edificio.add(caja2)

    var caja3Geometria = new THREE.BoxGeometry(40, 40, 40);
    var caja3Material = new THREE.MeshBasicMaterial({ color: 0xd4760b, wireframe: false });
    var caja3 = new THREE.Mesh(caja3Geometria, caja3Material);
    caja3.position.x = 0;
    caja3.position.y = 00;
    caja3.position.z = 80;
    edificio.add(caja3)

    var caja4Geometria = new THREE.BoxGeometry(40, 40, 40);
    var caja4Material = new THREE.MeshBasicMaterial({ color: 0xd4760f, wireframe: false });
    var caja4 = new THREE.Mesh(caja4Geometria, caja4Material);
    caja4.position.x = -40;
    caja4.position.y = 0;
    caja4.position.z = 120;
    edificio.add(caja4)

    var caja5Geometria = new THREE.BoxGeometry(60, 40, 120);
    var caja5Material = new THREE.MeshBasicMaterial({ color: 0xd4760b, wireframe: false });
    var caja5 = new THREE.Mesh(caja5Geometria, caja5Material);
    caja5.position.x = -50;
    caja5.position.y = 0;
    caja5.position.z = 40;
    edificio.add(caja5)

    var caja6Geometria = new THREE.BoxGeometry(100, 40, 30);
    var caja6Material = new THREE.MeshBasicMaterial({ color: 0xd4760b, wireframe: false });
    var caja6 = new THREE.Mesh(caja6Geometria, caja6Material);
    caja6.position.x = -90;
    caja6.position.y = 0;
    caja6.position.z = 100;
    edificio.add(caja6)

    var caja7Geometria = new THREE.BoxGeometry(100, 40, 30);
    var caja7Material = new THREE.MeshBasicMaterial({ color: 0xd4760b, wireframe: false });
    var caja7 = new THREE.Mesh(caja7Geometria, caja7Material);
    caja7.position.x = -90;
    caja7.position.y = 0;
    caja7.position.z = 40;
    edificio.add(caja7)

    var caja8Geometria = new THREE.BoxGeometry(100, 40, 30);
    var caja8Material = new THREE.MeshBasicMaterial({ color: 0xd4760b, wireframe: false });
    var caja8 = new THREE.Mesh(caja8Geometria, caja8Material);
    caja8.position.x = -90;
    caja8.position.y = 0;
    caja8.position.z = -10;
    edificio.add(caja8)
}

function contruirPlazoleta() {
    /* var planoGeometry = new THREE.PlaneGeometry(100, 100);
     var planoMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000 });
     var plano = new THREE.Mesh(planoGeometry, planoMaterial);
     console.log("plaolet")
     plano.position.x = 50;
     plano.position.y = 0;
     plano.position.z = -50;
     plano.rotateX(Math.PI / 2);
     escenario.add(plano);
 */
    var caja8Geometria = new THREE.BoxGeometry(200, 0, 200);
    var caja8Material = new THREE.MeshBasicMaterial({ color: 0xff0000, wireframe: false });
    var caja8 = new THREE.Mesh(caja8Geometria, caja8Material);
    caja8.position.x = 250;
    caja8.position.y = 0;
    caja8.position.z = 150;
    escenario.add(caja8)

    var cajGeometria = new THREE.BoxGeometry(100, 0, 180);
    var cajMaterial = new THREE.MeshBasicMaterial({ color: 255, wireframe: true });
    var caj = new THREE.Mesh(cajGeometria, cajMaterial);
    caj.position.x = 220;
    caj.position.y = 0;
    caj.position.z = -100;
    escenario.add(caj)

    var caj1Geometria = new THREE.BoxGeometry(100, 0, 180);
    var caj1Material = new THREE.MeshBasicMaterial({ color: 255, wireframe: true });
    var caj1 = new THREE.Mesh(caj1Geometria, caj1Material);
    caj1.position.x = 340;
    caj1.position.y = 0;
    caj1.position.z = -100;
    escenario.add(caj1)
}


function crearPiramide() {
    console.log("dd")
    var geometry = new THREE.CylinderGeometry(8, 8 * 8, 8 * 8, 8);
    var material = new THREE.MeshBasicMaterial({ color: 33370, wireframe: false });
    var cylinder = new THREE.Mesh(geometry, material);
    escenario.add(cylinder);
    cylinder.position.x = 240;
    cylinder.position.y = 40;
    cylinder.position.z = 190;
}

function contruirOtrosElementos() {
    //Esfera(radius : Float, widthSegments : Integer, heightSegments :
    var esferaGeometria = new THREE.SphereGeometry(5, 20, 60);
    var esferaMaterial = new THREE.MeshBasicMaterial({ color: 16776960, wireframe: true });
    var esfera = new THREE.Mesh(esferaGeometria, esferaMaterial);
    esfera.position.x = 200;
    esfera.position.y = 20;
    esfera.position.z = 80;

    escenario.add(esfera);

    var esfera2Geometria = new THREE.SphereGeometry(10, 10, 10);
    var esfera2Material = new THREE.MeshBasicMaterial({ color: 0x00fee0, wireframe: true });
    var esfera2 = new THREE.Mesh(esfera2Geometria, esfera2Material);
    esfera2.position.x = 10;
    esfera2.position.y = 63;
    esfera2.position.z = 0;

    escenario.add(esfera2);
    //Cono
    var conoGeometria = new THREE.ConeGeometry(30, 40, 30);
    var conoMaterial = new THREE.MeshBasicMaterial({ color: 0x00fee0, wireframe: true });
    var cono = new THREE.Mesh(conoGeometria, conoMaterial);
    cono.position.x = 60;
    cono.position.y = 0;
    cono.position.z = -60;

    escenario.add(cono);

    //Cono
    var cono1Geometria = new THREE.ConeGeometry(10, 20, 10);
    var cono1Material = new THREE.MeshBasicMaterial({ color: 1578780, wireframe: true });
    var cono1 = new THREE.Mesh(cono1Geometria, cono1Material);
    cono1.position.x = 200;
    cono1.position.y = 15;
    cono1.position.z = 80;

    escenario.add(cono1);

    var cilindroGeometria = new THREE.CylinderGeometry(30, 30, 0, 32);
    var cilindroMaterial = new THREE.MeshBasicMaterial({ color: 4036884 });
    var cilindro = new THREE.Mesh(cilindroGeometria, cilindroMaterial);

    cilindro.position.x = 50;
    cilindro.position.y = 0;
    cilindro.position.z = 60;
    escenario.add(cilindro)

    var cilindro1Geometria = new THREE.CylinderGeometry(2, 2, 40, 32);
    var cilindro1Material = new THREE.MeshBasicMaterial({ color: 128, wireframe: true });
    var cilindro1 = new THREE.Mesh(cilindro1Geometria, cilindro1Material);

    cilindro1.position.x = 30;
    cilindro1.position.y = 15;
    cilindro1.position.z = 60;

    escenario.add(cilindro1)

    var cilindro2Geometria = new THREE.CylinderGeometry(2, 2, 40, 32);
    var cilindro2Material = new THREE.MeshBasicMaterial({ color: 128, wireframe: true });
    var cilindro2 = new THREE.Mesh(cilindro2Geometria, cilindro2Material);

    cilindro2.position.x = 38;
    cilindro2.position.y = 15;
    cilindro2.position.z = 75;

    escenario.add(cilindro2)

    var cilindro3Geometria = new THREE.CylinderGeometry(2, 2, 40, 32);
    var cilindro3Material = new THREE.MeshBasicMaterial({ color: 128, wireframe: true });
    var cilindro3 = new THREE.Mesh(cilindro3Geometria, cilindro3Material);

    cilindro3.position.x = 38;
    cilindro3.position.y = 15;
    cilindro3.position.z = 50;

    escenario.add(cilindro3)
}

function crearPlano() {

    var x = new THREE.Vector3(10, 0, 0);
    var y = new THREE.Vector3(0, 10, 0);
    var z = new THREE.Vector3(0, 0, 10);


    var size = 10;
    var arrowSize = 1;
    var divisions = size;

    var color1 = new THREE.Color(0xFFFFFF);
    var color2 = new THREE.Color(0x333333);
    var colorR = new THREE.Color(0xAA0000);
    var colorG = new THREE.Color(0x00AA00);
    var colorB = new THREE.Color(0x0000AA);
    var colorRd = new THREE.Color(0xAA6666);
    var colorGd = new THREE.Color(0x66AA66);
    var colorBd = new THREE.Color(0x6666AA);

    //CREAR LAS GRILLAS PARA EL ESCENARIO
    var axesHelper1 = new THREE.AxesHelper(size / 10);
    var axesHelper2 = new THREE.AxesHelper(size / 10);
    var axesHelper3 = new THREE.AxesHelper(size / 10);

    var gridHelperXY = new THREE.GridHelper(size, divisions, color1, color1);
    var gridHelperXZ = new THREE.GridHelper(size, divisions, color2, color2);
    var gridHelperYZ = new THREE.GridHelper(size, divisions, color1, color2);

    //ROTARLAS PARA QUE QUEDEN EN EL ESPACIO ADECUADO
    gridHelperXY.rotateOnWorldAxis(x, THREE.Math.degToRad(90));
    gridHelperXZ.rotateOnWorldAxis(y, THREE.Math.degToRad(90));
    gridHelperYZ.rotateOnWorldAxis(z, THREE.Math.degToRad(90));

    //CREAR LAS FLECHAS QUE INDICAN LOS EJES DE COORDENADAS 3D
    var arrowX = new THREE.ArrowHelper(x, origin, arrowSize, colorR);
    var arrowY = new THREE.ArrowHelper(y, origin, arrowSize, colorG);
    var arrowZ = new THREE.ArrowHelper(z, origin, arrowSize, colorB);

    //AGREGAR A LA ESCENA
    escenario.add(gridHelperXZ);

    escenario.add(arrowX);
    escenario.add(arrowY);
    escenario.add(arrowZ);
}

function animate() {
    //animacion de camara/
    render();
    requestAnimationFrame(animate); /* funcion siempre me la este ejecutando*/
}

var onKeyDown = function (event) {
    switch (event.keyCode) {
        case 38: // TRASLADAR ADELANTE
            upArrow = true;

            break;
        case 40: // TRASLADAR ATRÁS
            downArrow = true;
            break;
        case 83: // ESCALA AGRANDAR
            scaleUp = true;
            break;
        case 87: // ESCALA DISMINUIR
            scaleDown = true;
            break;
        case 37: // ROTAR CW
            leftArrow = true;
            break;
        case 39: // ROTAR CCW
            rightArrow = true;
            break;
        case 88: // SELECCIONAR EJE X DE ROTACION
            yAxis = false;
            zAxis = false;
            xAxis = true;
            break;
        case 89: // SELECCIONAR EJE Y DE ROTACION
            xAxis = false;
            zAxis = false;
            yAxis = true;
            break;
        case 90: // SELECCIONAR EJE Z DE 
            xAxis = false;
            yAxis = false;
            zAxis = true;
            break;
    }
};

var onKeyUp = function (event) {
    switch (event.keyCode) {
        case 38: // TRASLADAR
            upArrow = false;
            break;
        case 40: // TRASLADAR
            downArrow = false;
            break;
        case 37: // ROTAR CW
            leftArrow = false;
            break;
        case 39: // ROTAR CCW
            rightArrow = false;
            break;
        case 83: // ESCALA AGRANDAR
            scaleUp = false;
            break;
        case 87: // ESCALA DISMINUIR
            scaleDown = false;
            break;
    }
};

function degrees_to_radians(degrees) {
    var pi = Math.PI;
    return degrees * (pi / 180);
}

var a = 45;
var b = 45;

function render() {
    render2();
    if (upArrow) {
        a = a + 1;
    }
    if (downArrow) {
        a = a - 1;
    }
    if (rightArrow) {
        b = b + 1;
    }
    if (leftArrow) {
        b = b - 1;
    }

    ro = THREE.Math.degToRad(a);
    thetha = THREE.Math.degToRad(b);


    camara.position.x = r * Math.cos(ro) * Math.sin(thetha);//coordenadas esefericas/
    camara.position.y = r * Math.sin(ro) * Math.sin(thetha);
    camara.position.z = r * Math.cos(ro);
    // console.log(r * Math.cos(ro) * Math.sin(thetha) + " y  " + r * Math.cos(ro) + "  z " + r * Math.sin(ro) * Math.sin(thetha));
    camara.lookAt(escenario.position);
    pantalla.render(escenario, camara);

}

function init() {
    console.log("Se inicia");
    pantalla = new THREE.WebGLRenderer();
    var WIDTH = window.innerWidth; // Ancho de pantalla
    var HEIGHT = window.innerHeight; // Alto de pantalla

    //Tamaño del render(resultado)
    pantalla.setSize(WIDTH, HEIGHT);
    escenario = new THREE.Scene();

    //La cámara
    var FOV = 70; //Field of view estando entre los 40º y 50ºun ángulo natural
    var aspect_ratio = window.innerWidth / window.innerHeight;
    var cerca = 0.1;
    var lejos = 1000; // frustrum 

    camara = new THREE.PerspectiveCamera(FOV, aspect_ratio, cerca, lejos);
    escenario.add(camara);
    document.getElementById("lienzo").appendChild(pantalla.domElement);
}

function crearEjes() {
    var ejes = new THREE.AxisHelper(100);
    escenario.add(ejes);
}

function crearAve() {

    cubo = new THREE.BoxGeometry(1, 1, 1);

    var colorcafe = new THREE.Color(0x4b3621);
    var materialCafe = new THREE.MeshBasicMaterial({ color: colorcafe, wireframe: false });

    cadera = new THREE.Mesh(cubo, materialCafe);
    cadera.position.x = 0;
    cadera.position.y = 5;
    cadera.position.z = 0;
    piernas = [];

    for (var n = 0; n < 2; n++) {

        let par = 0;
        let p = crearPierna();

        if (n % 2 === 0) {
            p.rotateX(degrees_to_radians(30 * (par - 1.5)));
            p.applyMatrix(new THREE.Matrix4().makeTranslation(-1, -.75, .5 + -.5 * par));
            p.rotateY(degrees_to_radians(0));
            p.rotateZ(degrees_to_radians(-60));
        } else {
            p.applyMatrix(new THREE.Matrix4().makeTranslation(1, -.75, .5 + -.5 * par));
            p.rotateX(degrees_to_radians(30 * (par - 1.5)));
            p.rotateZ(degrees_to_radians(60));
            par++;
        }
        cadera.add(p);
        cadera.applyMatrix(new THREE.Matrix4().makeScale(5, 5, 7.5));
        piernas.push(p);
    }

    escenario.add(cadera);
}

function crearPierna() {
    //CREAR LAS GEOMETRÍAS
    cilindro = new THREE.CylinderGeometry(.1, .3, 1.5, 32);

    var materialPierna = new THREE.MeshBasicMaterial({ color: new THREE.Color(0x0000ff) });

    return new THREE.Mesh(cilindro, materialPierna);
}

function render2() {
    var dtime = Date.now() - startTime;
    var tx = 0, ty = 0, tz = 0;	//Variables para traslacion
    var sc = 1;				//Variable para escala
    var theta = 0;			//Variable para ángulo de rotacion de piernas


    var sigma = 0;			//Variable para ángulo de rotación de caderas


    if (thetaSum >= 30 * Math.PI / 180)
        positivo = false;
    if (thetaSum <= -30 * Math.PI / 180)
        positivo = true;



    //acciones de la teclas 



    let tranlaZ = (Math.cos(THREE.Math.degToRad(anguloDireecion)));
    let tranlaX = (Math.sin(THREE.Math.degToRad(anguloDireecion)));

    console.log(tranlaZ + ", " + tranlaX);

    if (upArrow) {
        cadera.position.x = cadera.position.x + tranlaX / 1;
        cadera.position.z = cadera.position.z + tranlaZ / 1;
        tx = cadera.position.x; ty = 0; tz = .1;
        if (positivo)
            theta = .1;
        else
            theta = -.1;
    }
    if (downArrow) {
        cadera.position.x = cadera.position.x - tranlaX / 1;
        cadera.position.z = cadera.position.z - tranlaZ / 1;
        tx = cadera.position.x; ty = 0; tz = -.1;
        if (positivo)
            theta = .1;
        else
            theta = -.1;
    }
    thetaSum += theta;

    if (rightArrow) {
        sigma = -.1;
        anguloDireecion = anguloDireecion - 5.5
        console.log(anguloDireecion);
    }
    if (leftArrow) {
        anguloDireecion = anguloDireecion + 5.5

        console.log(anguloDireecion);
        sigma = .1;
    }

    //MATRIZ DE TRASLACIÓN
    var t = new THREE.Matrix4();
    t.set(1, 0, 0, tx,
        0, 1, 0, ty,
        0, 0, 1, tz,
        0, 0, 0, 1);

    cadera.matrix.multiply(t); 	//APLICAR LA TRASLACIÓN A NIVEL LOCAL

    //ROTACIONES
    var ct1 = Math.cos(theta);
    var ct2 = Math.cos(-theta);
    var cs = Math.cos(sigma);
    var st1 = Math.sin(theta);
    var st2 = Math.sin(-theta);
    var ss = Math.sin(sigma);

    var r = new THREE.Matrix4();
    var r1 = new THREE.Matrix4();
    var r2 = new THREE.Matrix4();

    //MATRIZ DE ROTACIÓN EN EJE Y
    r.set(cs, 0, ss, 0,
        0, 1, 0, 0,
        -ss, 0, cs, 0,
        0, 0, 0, 1);
    //MATRICES DE ROTACIÓN EN EJE LOCAL DE PIERNAS	
    r1.set(1, 0, 0, 0,
        0, ct1, -st1, 0,
        0, st1, ct1, 0,
        0, 0, 0, 1);
    r2.set(1, 0, 0, 0,
        0, ct2, -st2, 0,
        0, st2, ct2, 0,
        0, 0, 0, 1);

    //ROTACION EN UN EJE PARALELO
    var tempMatrix = new THREE.Matrix4();
    tempMatrix.copyPosition(cadera.matrix);
    cadera.applyMatrix(new THREE.Matrix4().getInverse(tempMatrix));
    cadera.applyMatrix(r);
    cadera.applyMatrix(tempMatrix);



    piernas[0].applyMatrix(r1);
    piernas[1].applyMatrix(r2);



}

var thetaSum = 0;
var positivo = false;
////////  empieza 
var camara;
var escenario;
var cadera;

//ELEMENTOS DE ESCENA
var origin = new THREE.Vector3(0, 0, 0);
var startTime = Date.now();

//TECLADO
var upArrow = false;
var downArrow = false;
var leftArrow = false;
var rightArrow = false;
var scaleUp = false;
var scaleDown = false;
var xAxis = true;
var yAxis = false;
var zAxis = false;


var anguloDireecion = 0; // lo usamos para calcular el angulo 

// variable coordenas esfericas 
var thetha = Math.PI / 3;   /* x a y */
var ro = Math.PI / 3; //x a z/
var r = 500; //radio/


init();
crearEjes(); // r x  v y b = z 
crearPlano();

contruirEdificios();
contruirPlazoleta();
contruirOtrosElementos();
crearPiramide();
crearAve();

document.addEventListener('keydown', onKeyDown, false);
document.addEventListener('keyup', onKeyUp, false);
animate();