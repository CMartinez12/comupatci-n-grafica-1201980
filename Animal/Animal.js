var scene, aspect, camera, renderer, controls;
init();
construirAnimal();

animate();

function init() {
    //INICIALIZACIÓN
    scene = new THREE.Scene();
    aspect = window.innerWidth / window.innerHeight;
    camera = new THREE.PerspectiveCamera(75, aspect, 0.1, 1000);
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    var renderHTML = document.getElementById("render");
    renderHTML.appendChild(renderer.domElement);
    controls = new THREE.OrbitControls(camera, renderer.domElement);

    //ELEMENTOS COMUNES
    var size = 10;
    var arrowSize = 1;
    var divisions = size;
    var origin = new THREE.Vector3(0, 0, 0);
    var x = new THREE.Vector3(1, 0, 0);
    var y = new THREE.Vector3(0, 1, 0);
    var z = new THREE.Vector3(0, 0, 1);
    //ELEMENTOS DE ESCENA
    var size = 10;
    var arrowSize = 1;
    var divisions = size;
    var origin = new THREE.Vector3(0, 0, 0);
    var x = new THREE.Vector3(1, 0, 0);
    var y = new THREE.Vector3(0, 1, 0);
    var z = new THREE.Vector3(0, 0, 1);
    var color1 = new THREE.Color(0xFFFFFF);
    var color2 = new THREE.Color(0x333333);
    var colorR = new THREE.Color(0xAA3333);
    var colorG = new THREE.Color(0x33AA33);
    var colorB = new THREE.Color(0x333366);
    var colorRd = new THREE.Color(0xAA6666);
    var colorGd = new THREE.Color(0x66AA66);
    var colorBd = new THREE.Color(0x6666AA);

    //CREAR LAS GRILLAS PARA EL ESCENARIO
    var axesHelper = new THREE.AxesHelper(size);
    var gridHelperXY = new THREE.GridHelper(size, divisions, color1, color1);
    var gridHelperXZ = new THREE.GridHelper(size, divisions, color2, color2);
    var gridHelperYZ = new THREE.GridHelper(size, divisions, color2, color2);

    //ROTARLAS PARA QUE QUEDEN EN EL ESPACIO ADECUADO
    gridHelperXY.rotateOnWorldAxis(x, THREE.Math.degToRad(90));
    gridHelperXZ.rotateOnWorldAxis(y, THREE.Math.degToRad(90));
    gridHelperYZ.rotateOnWorldAxis(z, THREE.Math.degToRad(90));

    //CREAR LAS FLECHAS QUE INDICAN LOS EJES DE COORDENADAS 3D
    var arrowX = new THREE.ArrowHelper(x, origin, arrowSize, colorR);
    var arrowY = new THREE.ArrowHelper(y, origin, arrowSize, colorG);
    var arrowZ = new THREE.ArrowHelper(z, origin, arrowSize, colorB);

    //CREAR ILUMINACIÓN 
    /*var ambient = new THREE.AmbientLight( 0xffffff, 2 );
    scene.add( ambient );
	
    var pointLight = new THREE.PointLight( 0xffffff, 1, 100 );
    pointLight.position.set( -5, -2, 5 );
    scene.add( pointLight );
	
    var pointLight = new THREE.PointLight( 0xffffff, .5, 100 );
    pointLight.position.set( 5, -2, -5 );
    scene.add( pointLight );*/

    //AGREGAR A LA ESCENA
    scene.add(gridHelperXZ);
    scene.add(arrowX);
    scene.add(arrowY);
    scene.add(arrowZ);

    //MOVER LA CÁMARA
    camera.position.x = 0;
    camera.position.y = 0;
    camera.position.z = 20;
    camera.lookAt(origin);
}

function construirAnimal() {
    solidos3D();// piernas
    iluminar();
    estrude();
    curvaBeizer();
    fan_strip(); // cola triangulo
    poligono3D(); // caparazon
    var p1 =  solidos3D();// piernas
    p1.position.set(1,0,0);
    p1.position.x=1.5;
    p1.position.z=0.8;
    scene.add(p1);  
    var p2 =  solidos3D();// piernas
    p2.position.set(1,0,0); 
    p2.position.x=1.5;
    p2.position.z=-0.8;
    scene.add(p2); 
    var p3 =  solidos3D();// piernas
    p3.position.set(1,0,0); 
    p3.position.x=-1.5;
    p3.position.z=-0.8;
    p3.rotation.y= -600;
    scene.add(p3); 
    var p4 =  solidos3D();// piernas
    p4.position.set(1,0,0); 
    p4.position.x= -1.5;
    p4.position.z=- -1;
    p4.rotation.y= -600;
    scene.add(p4); 



    function poligono3D() {
        var geometry = new THREE.SphereGeometry(1.5, 10, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0xffff00, wireframe:true });
        var sphere = new THREE.Mesh(geometry, material);
        //scene.add(sphere);

        var geometry = new THREE.BoxGeometry(2.7, 1.5, 2.7);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var cube = new THREE.Mesh(geometry, material);
        cube.position.y = -0.8;
        //scene.add(cube);

        var spCSG = THREE.CSG.fromMesh(sphere);
        var boxCSG = THREE.CSG.fromMesh(cube);

        var resultado = spCSG.subtract(boxCSG);


        var caparazon = THREE.CSG.toMesh(resultado);
        caparazon.material = new THREE.MeshStandardMaterial({ color: 0x00cc11 });
        scene.add(caparazon);





    }

    function fan_strip() {
        //GEOMETRIA
        var geoTriangulo = new THREE.Geometry();
        //VERTICES


        var v0 = new THREE.Vector3(4, 4, 4);
        var v1 = new THREE.Vector3(5, 0, 0);
        var v2 = new THREE.Vector3(3, 5, 0);
        var v3 = new THREE.Vector3(2, 5, 3);
        var v4 = new THREE.Vector3(5, 0, 2);

        geoTriangulo.vertices.push(v0);
        geoTriangulo.vertices.push(v1);
        geoTriangulo.vertices.push(v2);
        geoTriangulo.vertices.push(v3);
        geoTriangulo.vertices.push(v4);
        //CARAS
        // unimos las caras ppor medio de los vertices con el indice de indezado 
        geoTriangulo.faces.push(new THREE.Face3(0, 1, 2))
        geoTriangulo.faces.push(new THREE.Face3(0, 2, 3));
        geoTriangulo.faces.push(new THREE.Face3(0, 3, 4));
        geoTriangulo.faces.push(new THREE.Face3(0, 4, 1));

        geoTriangulo.computeFaceNormals();
        //MATERIAL Y CREACION DEL OBJETO
        var material = new THREE.MeshStandardMaterial({ color: 0x00cc00 });

        var triangulo = new THREE.Mesh(geoTriangulo, material);
        triangulo.rotation.x = -Math.PI / 2;
        triangulo.rotation.z = Math.PI / 2;
        triangulo.position.x = 0;
        triangulo.position.y = -5;
        triangulo.position.y = 1;

        scene.add(triangulo);

    }

    function solidos3D() {
        var geometry = new THREE.CylinderGeometry(0.3, 0.3, 1, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
        var cylinder = new THREE.Mesh(geometry, material);


        //scene.add(cylinder)
        var geometry2 = new THREE.CylinderGeometry(0.3, 0.3, 1, 32);
        var cylinder2 = new THREE.Mesh(geometry2, material);

        var geometry = new THREE.ConeGeometry(0.1, 0.8, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
        var cone = new THREE.Mesh(geometry, material);
        cone.position.x = .3;
        cone.position.y = -.5;

        cone.rotation.z = -Math.PI / 2;

        cone2 = cone.clone();
        cone3 = cone.clone();
        cone2.position.z = -.1;
        cone3.position.z = .1;
        cone2.rotation.y = Math.PI / 6;
        cone3.rotation.y = -Math.PI / 6;


        var geometry = new THREE.SphereGeometry(0.45, 20, 20);
        var material = new THREE.MeshBasicMaterial({ color: 0x6666AA});
        var sphere = new THREE.Mesh(geometry, material);





        sphere.position.y = 0.8;
        sphere.position.x = 0;
        cylinder2.rotation.z = Math.PI / 2;
        cylinder2.position.x = sphere.position.x - .5;
        cylinder2.position.y = sphere.position.y;

        

        //scene.add(sphere);


         //scene.add(cylinder2)
        // converitr a csg 
        var femurCSG =  THREE.CSG.fromMesh(cylinder2);
        var piernasCSG = THREE.CSG.fromMesh(cylinder);
        var rodillasCSG = THREE.CSG.fromMesh(sphere);
        var garra1CSG = THREE.CSG.fromMesh(cone);
        var garra2CSG = THREE.CSG.fromMesh(cone2);
        var garra3CSG = THREE.CSG.fromMesh(cone3);

     

        var resultado = piernasCSG.union(rodillasCSG);
        
        resultado = resultado.union(femurCSG);
        
        resultado = resultado.union(garra1CSG);
        resultado = resultado.union(garra2CSG);
        resultado = resultado.union(garra3CSG);
        
        var pierna = THREE.CSG.toMesh(resultado);
        
        pierna.material = new THREE.MeshBasicMaterial({ color: new THREE.Color('green') });
      
      

       // scene.add(pierna);

        return pierna.clone(); 
    }
   

    function iluminar() {
        var ambient = new THREE.AmbientLight(0xffffff, 1);
        scene.add(ambient);

        var pointLight1 = new THREE.PointLight(0xffffff, .2, 100);
        pointLight1.position.set(10, 10, -10);
        scene.add(pointLight1);
    }

    function estrude() {
        // puntos de la figuara 
        var curve2D = [];
        curve2D[0] = new THREE.Vector2(0, 4);
        curve2D[1] = new THREE.Vector2(2, 3.5);
        curve2D[2] = new THREE.Vector2(3.5, 4.5);
        curve2D[3] = new THREE.Vector2(5.5, 5);
        curve2D[4] = new THREE.Vector2(9, 3);
        curve2D[5] = new THREE.Vector2(9, 2);
        curve2D[6] = new THREE.Vector2(6, 0.5);
        curve2D[7] = new THREE.Vector2(0, 2);


        var shape = new THREE.Shape();
        shape.moveTo(0, 0);
        shape.splineThru(curve2D);

        var material = new THREE.LineBasicMaterial({ color: 0xff0000 });
        var resolution = 50;
        var points = shape.getPoints(resolution);

        var geometry = new THREE.BufferGeometry().setFromPoints(points);
        // Create the final object to add to the scene
        var curveLine = new THREE.Line(geometry, material);

        //EXTRUDE
        var extrudeSettings = {
            steps: 2,
            amount: 3,
            bevelEnabled: false,
        };

        var geometryExt = new THREE.ExtrudeGeometry(shape, extrudeSettings);
        var materialExt = new THREE.MeshStandardMaterial({
            color: 0x00ff00,
            metalness: 0.5,
            roughness: 0.1,
            opacity: 0.75,
            transparent: true
        });



        var meshGeneratedFromCurveLine = new THREE.Mesh(geometryExt, materialExt);
       // scene.add(curveLine);
        scene.add(meshGeneratedFromCurveLine);
        meshGeneratedFromCurveLine.scale.set(.3, .3, .3);
        meshGeneratedFromCurveLine.rotation.x = -Math.PI / 2;
        meshGeneratedFromCurveLine.rotation.y = Math.PI / 2;
        meshGeneratedFromCurveLine.rotation.z = Math.PI / 2;
        meshGeneratedFromCurveLine.position.x = -.3;
        meshGeneratedFromCurveLine.position.y = -0.4;
        meshGeneratedFromCurveLine.position.z = -0.2;

    }

    function curvaBeizer() {

        var curve1 = new THREE.CubicBezierCurve3(
            new THREE.Vector3(0, 0, 0),
            new THREE.Vector3(0, 3, 0),
            new THREE.Vector3(9, 5, 0),
            new THREE.Vector3(14, 0, 0)
        );

        var curve2 = new THREE.CubicBezierCurve3(
            new THREE.Vector3(0, 0, -5),
            new THREE.Vector3(0, 3, -5),
            new THREE.Vector3(9, 5, -5),
            new THREE.Vector3(14, 0, -5)
        );

        var pointGeometry = new THREE.Geometry();
        for (var i = 0; i < 20; i++) {
            var point = curve1.getPoint(i / 20);
            pointGeometry.vertices.push(point);
        }

        for (var i = 0; i < 20; i++) {
            var point = curve2.getPoint(i / 20);
            pointGeometry.vertices.push(point);
        }

        var pointMaterial = new THREE.PointsMaterial({ color: new THREE.Color('yellow') });
        var curvePoints = new THREE.Points(pointGeometry, pointMaterial);

        var material = new THREE.MeshStandardMaterial({ color: new THREE.Color('red') });
        material.side = THREE.DoubleSide;
        for (var i = 0; i < 19; i++) {
            var face1 = new THREE.Face3(i, i + 1, i + 20);
            pointGeometry.faces.push(face1);
            var face2 = new THREE.Face3(i + 1, i + 20, i + 21);
            pointGeometry.faces.push(face2);
        }
        pointGeometry.computeFaceNormals();
        //pointGeometry.computeVertexNormals();
        var curvaSuperficie = new THREE.Mesh(pointGeometry, material);
        curvaSuperficie.scale.set(.3, .3, .3); //tamaño
        curvaSuperficie.rotation.x = Math.PI / 2;
        curvaSuperficie.rotation.y = Math.PI / 2;
        curvaSuperficie.rotation.z = Math.PI / 2;
        curvaSuperficie.position.x = 0.8;
        curvaSuperficie.position.y = .1;
        curvaSuperficie.position.z = -2;
        scene.add(curvaSuperficie);
        //scene.add(curvePoints ); //puntos amarillos

    }

    /*
    function fan(){

    var lista = []; 
 var puntos = 10; 
    for(let i = 0; i<puntos; i++ ){
        var x1 = cos( )
        var puntoXYZ = new THREE.vector(x1, x2,x3); 
        lista.add(puntoXYZ); 


    
    }

        var geometry = new ConvexGeometry( points );
    var material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
    var mesh = new THREE.Mesh( geometry, material );
    scene.add( mesh )

    */

}


function animate() {
    requestAnimationFrame(animate);
    controls.update();
    render();
}

function render() {
    renderer.render(scene, camera);
}