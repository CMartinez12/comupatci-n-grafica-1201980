console.log("Se inicia");
var pantalla = new THREE.WebGLRenderer();
var WIDTH = window.innerWidth; // Ancho de pantalla
var HEIGHT = window.innerHeight; // Alto de pantalla

document.getElementById("lienzo").appendChild(pantalla.domElement);

    //Tamaño del render(resultado)
    pantalla.setSize(WIDTH,HEIGHT);

var escenario = new THREE.Scene();
 //La cámara
 var FOV=45; //Field of view estando entre los 40º y 50ºun ángulo natural
 var aspect_ratio  = window.innerWidth / window.innerHeight;
 var cerca = 0.1;
 var lejos = 1000; // frustrum 
 
 var camara=new THREE.PerspectiveCamera(FOV, aspect_ratio,cerca,lejos );
escenario.add(camara);

var planoGeometry = new THREE.PlaneGeometry(60,20); 
var planoMaterial = new THREE.MeshBasicMaterial({color: 0xcccccc});
var plano = new THREE.Mesh(planoGeometry,planoMaterial);

plano.position.x = 0;
plano.position.y = 0;
plano.position.z = 0;

escenario.add(plano);

var ejes = new THREE.AxisHelper(100);

escenario.add(ejes);

var cajaGeometria = new THREE.BoxGeometry(10,20,30);
var cajaMaterial = new THREE.MeshBasicMaterial({color: 0x00ff00, wireframe:true});
var caja = new THREE.Mesh(cajaGeometria,cajaMaterial);

escenario.add(caja)

//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments :
var esferaGeometria = new THREE.SphereGeometry(5,20,20);
var esferaMaterial = new THREE.MeshBasicMaterial({color: 0x00fee0, wireframe:true});
var esfera = new THREE.Mesh(esferaGeometria,esferaMaterial);
esfera.position.x = 0;
esfera.position.y = 55;
esfera.position.z = 0;

escenario.add(esfera);

//SphereGeometry(radius : Float, widthSegments : Integer, heightSegments :
var esfera2Geometria = new THREE.SphereGeometry(2,7,7);
var esfera2Material = new THREE.MeshBasicMaterial({color: 0x00fee0, wireframe:true});
var esfera2 = new THREE.Mesh(esfera2Geometria,esfera2Material);
esfera2.position.x = 0;
esfera2.position.y = 63;
esfera2.position.z = 0;

escenario.add(esfera2);


var  colorc = new THREE.Color("rgb(20 , 110, 90)");

var conoGeometria = new THREE.ConeGeometry(5,60,40);
var conoMaterial = new THREE.MeshBasicMaterial({color: colorc, wireframe:true});
var cono = new THREE.Mesh(conoGeometria,conoMaterial);
cono.position.x = 0;
cono.position.y = 45;
cono.position.z = 0;

escenario.add(cono);

camara.position.x = 130;
camara.position.y = 130;
camara.position.z = 50;

camara.lookAt(escenario.position);
pantalla.render(escenario,camara);




