

var camera, scene, renderer;
var plane;
var mouse, raycaster, isShiftDown = false;

var rollOverMesh, rollOverMaterial;
var cubeGeo, cubeMaterial;

var objects = [];
var indexTipoObjeto = 0;

var nombreObjeto = "Figura";
function devolverObjeto() {

    var cantidadObjetos = 6;
    var objetoM = null;
    nombreObjeto = "Cubo";
    if (indexTipoObjeto % cantidadObjetos == 0) {

        objeto = new THREE.Mesh(cubeGeo, cubeMaterial);
        console.log("Cubo");
    }
    nombreObjeto = "Beizer";
    if (indexTipoObjeto % cantidadObjetos == 1) {
    var curve1 = new THREE.CubicBezierCurve3(
        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(0, 3, 0),
        new THREE.Vector3(9, 5, 0),
        new THREE.Vector3(14, 0, 0)
    );

    var curve2 = new THREE.CubicBezierCurve3(
        new THREE.Vector3(0, 0, -5),
        new THREE.Vector3(0, 3, -5),
        new THREE.Vector3(9, 5, -5),
        new THREE.Vector3(14, 0, -5)
    );

    var pointGeometry = new THREE.Geometry();
    for (var i = 0; i < 20; i++) {
        var point = curve1.getPoint(i / 20);
        pointGeometry.vertices.push(point);
    }

    for (var i = 0; i < 20; i++) {
        var point = curve2.getPoint(i / 20);
        pointGeometry.vertices.push(point);
    }

    var pointMaterial = new THREE.PointsMaterial({ color: new THREE.Color('yellow') });
    var curvePoints = new THREE.Points(pointGeometry, pointMaterial);

    var material = new THREE.MeshStandardMaterial({ color: new THREE.Color('red') });
    material.side = THREE.DoubleSide;
    for (var i = 0; i < 19; i++) {
        var face1 = new THREE.Face3(i, i + 1, i + 20);
        pointGeometry.faces.push(face1);
        var face2 = new THREE.Face3(i + 1, i + 20, i + 21);
        pointGeometry.faces.push(face2);
    }
    pointGeometry.computeFaceNormals();
    pointGeometry.computeVertexNormals();
    var curvaSuperficie = new THREE.Mesh(pointGeometry, material);
    objeto = curvaSuperficie;
    console.log("Beizer");
    objeto.scale.set(20, 20, 20);
  }
    nombreObjeto = "Solido";
    if (indexTipoObjeto % cantidadObjetos == 2) {
        var geometry = new THREE.SphereGeometry(1.5, 10, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0xffff00, wireframe: true });
        var sphere = new THREE.Mesh(geometry, material);
        //scene.add(sphere);

        var geometry = new THREE.BoxGeometry(2.7, 1.5, 2.7);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var cube = new THREE.Mesh(geometry, material);
        cube.position.y = -0.8;
        //scene.add(cube);

        var spCSG = THREE.CSG.fromMesh(sphere);
        var boxCSG = THREE.CSG.fromMesh(cube);

        var resultado = spCSG.subtract(boxCSG);
        objeto = THREE.CSG.toMesh(resultado);
        objeto.material = new THREE.MeshStandardMaterial({ color: 0x00cc11 });
        console.log("Copula");

        objeto.scale.set(30, 30, 30);

    }
    nombreObjeto = "Extrude";
    if (indexTipoObjeto % cantidadObjetos == 3) {

        var curve2D = [];
        curve2D[0] = new THREE.Vector2(0, 4);
        curve2D[1] = new THREE.Vector2(2, 3.5);
        curve2D[2] = new THREE.Vector2(3.5, 4.5);
        curve2D[3] = new THREE.Vector2(5.5, 5);
        curve2D[4] = new THREE.Vector2(9, 3);
        curve2D[5] = new THREE.Vector2(9, 2);
        curve2D[6] = new THREE.Vector2(6, 0.5);
        curve2D[7] = new THREE.Vector2(0, 2);


        var shape = new THREE.Shape();
        shape.moveTo(0, 0);
        shape.splineThru(curve2D);

        var material = new THREE.LineBasicMaterial({ color: 0xff0000 });
        var resolution = 50;
        var points = shape.getPoints(resolution);

        var geometry = new THREE.BufferGeometry().setFromPoints(points);
        // Create the final object to add to the scene
        var curveLine = new THREE.Line(geometry, material);

        //EXTRUDE
        var extrudeSettings = {
            steps: 2,
            amount: 3,
            bevelEnabled: false,
        };

        var geometryExt = new THREE.ExtrudeGeometry(shape, extrudeSettings);
        var materialExt = new THREE.MeshStandardMaterial({
            color: 0x00FFFF00,
            metalness: 0.5,
            roughness: 0.1,
            opacity: 0.75,
            transparent: true
        });

        var meshGeneratedFromCurveLine = new THREE.Mesh(geometryExt, materialExt);
        objeto = meshGeneratedFromCurveLine;
        console.log("Extrude");
        objeto.scale.set(20, 20, 20);

    }

    nombreObjeto = "Solido";
    if (indexTipoObjeto % cantidadObjetos == 4) {
        var geometry = new THREE.SphereGeometry(.5, 32, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0xffff00, wireframe: true });
        var sphere = new THREE.Mesh(geometry, material);

        sphere.translateX( .5 );
        sphere.translateY( .5 );
        sphere.translateZ( .5 );
        //scene.add(sphere);

        var geometry = new THREE.BoxGeometry(1, 1, 1);
        var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        var cube = new THREE.Mesh(geometry, material);
        
        //scene.add(cube);

        var sphereCSG = THREE.CSG.fromMesh(sphere);
        var boxCSG = THREE.CSG.fromMesh(cube);
        var resultado = boxCSG.union(sphereCSG);
        objeto = THREE.CSG.toMesh(resultado);
        objeto.material = new THREE.MeshStandardMaterial({ color: 0x00FF7F7F });
        console.log("Solido");

        objeto.scale.set(50, 50, 50);
      }
      nombreObjeto = "Fan";
    if (indexTipoObjeto % cantidadObjetos == 5) {
      var geoTriangulo = new THREE.Geometry();
        //VERTICES
        var v0 = new THREE.Vector3(4, 4, 4);
        var v1 = new THREE.Vector3(5, 0, 0);
        var v2 = new THREE.Vector3(3, 5, 0);
        var v3 = new THREE.Vector3(2, 5, 3);
        var v4 = new THREE.Vector3(5, 0, 2);

        geoTriangulo.vertices.push(v0);
        geoTriangulo.vertices.push(v1);
        geoTriangulo.vertices.push(v2);
        geoTriangulo.vertices.push(v3);
        geoTriangulo.vertices.push(v4);
        //CARAS
        // unimos las caras ppor medio de los vertices con el indice de indezado 
        geoTriangulo.faces.push(new THREE.Face3(0, 1, 2))
        geoTriangulo.faces.push(new THREE.Face3(0, 2, 3));
        geoTriangulo.faces.push(new THREE.Face3(0, 3, 4));
        geoTriangulo.faces.push(new THREE.Face3(0, 4, 1));

        geoTriangulo.computeFaceNormals();
        //MATERIAL Y CREACION DEL OBJETO
        var material = new THREE.MeshStandardMaterial({ color: 0x007F7F7F });
        var triangulo = new THREE.Mesh(geoTriangulo, material);
        objeto = triangulo;
        console.log("Fan");
        objeto.scale.set(20, 20, 20);
  }
    return objeto;
}
///////////////////////////////////
init();
iluminar();
animate();
render();
//////////////////////////////////
function init() {

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.set(500, 800, 1300);
    camera.lookAt(0, 0, 0);

    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xf0f0f0);


    // roll-over helpers

    var rollOverGeo = new THREE.BoxBufferGeometry(50, 50, 50);
    rollOverMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000, opacity: 0.5, transparent: true });
    rollOverMesh = new THREE.Mesh(rollOverGeo, rollOverMaterial);
    scene.add(rollOverMesh);

    // Cubo

    cubeGeo = new THREE.BoxBufferGeometry(50, 50, 50);
    cubeMaterial = new THREE.MeshBasicMaterial( {color: 0x000000FF} );

    // Grillas

    var gridHelper = new THREE.GridHelper(1000, 20);
    scene.add(gridHelper);

    //

    raycaster = new THREE.Raycaster();
    mouse = new THREE.Vector2();

    var geometry = new THREE.PlaneBufferGeometry(1000, 1000);
    geometry.rotateX(- Math.PI / 2);

    plane = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ visible: false }));
    scene.add(plane);

    objects.push(plane);

    // lights

    var ambientLight = new THREE.AmbientLight(0x606060);
    scene.add(ambientLight);

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 0.75, 0.5).normalize();
    scene.add(directionalLight);

    renderer = new THREE.WebGLRenderer({ antialias: true });
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // Estructura de lister('nombreaccionNavegaoes', nombreFuncioEjectura, false)
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('keydown', onDocumentKeyDown, false);
    document.addEventListener('keyup', onDocumentKeyUp, false);
    //

    window.addEventListener('resize', onWindowResize, false);

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function onDocumentMouseMove(event) {

    event.preventDefault();

    mouse.set((event.clientX / window.innerWidth) * 2 - 1, - (event.clientY / window.innerHeight) * 2 + 1);

    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(objects);

    if (intersects.length > 0) {

        var intersect = intersects[0];

        rollOverMesh.position.copy(intersect.point).add(intersect.face.normal);
        rollOverMesh.position.divideScalar(50).floor().multiplyScalar(50).addScalar(25);

    }

    render();

}

function onDocumentMouseDown(event) {

    if (run) {
        event.preventDefault();

        mouse.set((event.clientX / window.innerWidth) * 2 - 1, - (event.clientY / window.innerHeight) * 2 + 1);

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            var intersect = intersects[0];

            // delete cube

            if (isShiftDown) {

                if (intersect.object !== plane) {

                    scene.remove(intersect.object);

                    objects.splice(objects.indexOf(intersect.object), 1);

                }

                // create cube

            } else {

                var objectoToAdd = devolverObjeto();
                objectoToAdd.position.copy(intersect.point).add(intersect.face.normal);
                objectoToAdd.position.divideScalar(50).floor().multiplyScalar(50).addScalar(25);
                scene.add(objectoToAdd);

                objects.push(objectoToAdd);

                // Modificar para cambiar elemento 

            }

            render();

        }
    } else {
        if (confirm("Inicia")) {
            run = true;
        }
    }
}
//BOTONES DE TECLAS
// arrow left	37
// arrow up	38
// arrow right	39
// arrow down	40
function onDocumentKeyDown(event) {

    switch (event.keyCode) {

        case 16: isShiftDown = true; break;
        case 38: indexTipoObjeto++; break;
        case 40: indexTipoObjeto--; break;

    }
    console.log(indexTipoObjeto)

}

function onDocumentKeyUp(event) {

    var s = document.getElementById("figura");
    s.innerHTML = nombreObjeto + " " + indexTipoObjeto;
    switch (event.keyCode) {

        case 16: isShiftDown = false; break;

    }

}

function render() {

    renderer.render(scene, camera);

}
function iluminar() {
    var ambient = new THREE.AmbientLight(0xffffff, 1);
    scene.add(ambient);

    var pointLight1 = new THREE.PointLight(0xffffff, .2, 100);
    pointLight1.position.set(10, 10, -10);
    scene.add(pointLight1);
}

function animate() {
    requestAnimationFrame(animate);
    controls.update();
    render();
}

var run = false;
function animateClick() {
    run = true;
}
function stopClick() {
    run = false;
}
